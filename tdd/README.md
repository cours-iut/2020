# TP TDD

# Théorie

## Le cycle "Red > Green > Refactor"

On applique un cycle simple : 

1. On crée un test validant la fonctionnalité. Souvent ce test ne compile pas. Il faut alors créer les classes et/ou les méthodes pour ce test compile. Néanmoins, on n'implémente rien. Le test doit pouvoir s'éxécuter et échouer. C'est la preuve que la fonctionnalité n'existe pas.
2. On implémente le code permettant au test de passer. Le but est d'écrire le code le plus simple possible. On ne cherche pas la beauté.
3. Le test étant validé, on peut refactorer la méthode (si besoin) en toute sécurité.

Attention lors de la troisième étape. Lorsqu'on effectue un refactoring, on peut être amené à faire des abstraction et des choix de design. Si ce sont de mauvais choix, le code sera inutilement complexe. Si vous avez un doute, laissez-vous un peu de temps : continuez à écrire quelques tests et effectuez le refactoring lorsque vous sûr de votre coup.

## SOLID

Si la méthodologie est simple, la pratique se révèle plus complexe car il vous faudra bien connaitre les différents moyens à votre disposition pour refactorer votre code.  
Une première piste est l'acronyme [**SOLID**](https://fr.wikipedia.org/wiki/SOLID_(informatique)) qui décrit 5 règles permettant de mieux concevoir, découper et découpler vos classes.

# Pratique

*Implémentez les spécifications suivantes en TDD*

## Attaquant et défenseur

Dans notre RPG, un joueur peut en attaquer un autre. On parlera alors de joueur **attaquant** et de joueur **défenseur**.  
L'attaque prend en compte certaines statistiques du joueur attaquant et de son arme pour calculer un nombre de dégâts infligés au joueur défenseur.  
Si le joueur défenseur dispose d'une armure, elle pourra absorber tout ou partie des dégâts infligés.  
Les dégâts non absorbés seront le nombre de points de vie (PV) perdus par le joueur défenseur.  
Les dégâts infligés et absorbés seront toujours arrondis à l'entier inférieur.

> Exemple :  
> L'attaquant inflige 10 dégâts et le défenseur absorbe 8 dégâts.  
> Le défenseur perd 2 PV.

## Dégâts infligés par une arme

Chaque arme dispose d'une **puissance de base**. 
Pour calculer les dégâts infligés par l'arme, on utilisera cette puissance de base qu'on multipliera par un **facteur critique**.
Le facteur critique est un pourcentage entre 80% et 120%, aléatoirement choisi à chaque attaque.

> Exemple avec une *épée courte (puissance : 10)* et un facteur critique de 80% :  
> `10 * 80% = 8 dégâts`

## Dégâts absorbés par une armure

Chaque armure dispose d'une **résistance de base**. 
Pour calculer les dégâts absorbés par l'armure, on utilisera cette résistance de base qu'on appliquera comme un pourcentage de réduction sur les dégâts.

> Exemple avec 10 dégâts infligés à une *cotte de maille (résistance : 20)* :  
> `10 * 20% = 2 dégâts absorbés`

## Armes perforantes

Certaines armes sont **perforantes**. 
Lorsqu'un joueur attaque avec l'une de ces armes, 25% des dégâts ignorent l'armure du joueur défenseur.
La règle de calcul de l'armure se base alors sur les 75% restants.

> Exemple avec une *arbalète (puissance : 100, perforante)*, un facteur critique de 100% et une *armure de chevalier (résistance: 90)* :  
> Dégâts infligés : `100 * 100% = 100 dégâts`  
> Dégâts non absorbables : `100 * 25% = 25 dégâts`  
> Dégâts absorbables : `100 - 25 = 75 dégâts`  
> Dégâts absorbés par l'armure : `75 * 90% = 67.5 => 67 dégâts`  
> Dégâts non absorbés : `75 - 67 = 8`  
> PV perdus : `25 + 8 = 33 PV`  
> Avec la règle normale, le défenseur aurait absorbé `100 * 90% = 90 dégâts` et aurait perdu : `100 - 90 = 10 PV`  

## Armures renforcées

Lorsqu'un joueur possédant une **armure renforcée** est attaqué par une arme perforante, la règle spéciale des armes perforantes ne s'applique pas. 
Lorsqu'il est attaqué avec une arme normale, l'armure absorbe 25% des dégâts non absorbés par la règle normale.

> Exemple avec une *hallebarde (puissance : 60)*, un facteur critique de 100% et une *armure de plaque (résistance: 70)* :  
> Dégâts infligés : `60 * 100% = 60 dégâts`  
> Dégâts absorbés (régle normale) : `60 * 70% = 42 dégâts`  
> Dégâts restants : `60 - 42 = 18 dégâts`  
> Avec la règle normale, le défenseur aurait donc percu 18 PV.  
> Dégâts supplémentaires absorbés : `18 * 25% = 4.5 => 4 dégâts`  
> Dégâts restants : `18 - 4 = 14 dégâts`  

## Bouclier

Dans certains cas, un joueur peut bénéficier d'un **bouclier**. Le bouclier permet d'absorber un nombre défini de dégâts. Lorsqu'un défenseur possédant un bouclier est attaqué, la règle d'armure est appliqué, puis le bouclier absorbe autant de dégâts que possible.

> Exemple avec un attaque infligeant 50 dégâts, une *cotte de maille (résistance : 20)* et un *bouclier mineur (protection: 10)* :  
> Dégâts absobrés par l'armure : `50 * 20% = 10 dégâts`  
> Dégâts restants : `50 - 20 = 30 dégâts`  
> Le bouclier absorbe 10 points de dégâts  
> Le défenseur perd 20 PV  

## Invulnérabilité

Dans certains cas, un joueur défenseur peut être **invulnérable**. Dans ce cas, peut importe l'attaque, il ne recevra aucun dégâts.

## Magie

Outre les armes, il est également possible d'attaquer un joueur à l'aide de **magie**. Pour calculer les dégâts de l'attaque, on utiliser la même règles pour les armes.  
En revanche, les attaques magiques **ignorent les armures traditionnelles**.

## Runes

Les armures peuvent être améliorées à l'aide de **runes**. Les runes ajoutent une **résistance magique** aux armures, leur permettant d'absorber des dégâts magiques.  
Pour calculer les dégâts absorbés, on utilisera la même règle que pour les armures traditionnelles.

## Attaques mixtes

Certaines armes exceptionnelles permettent d'infliger à la fois des dégâts normaux et des dégâts magiques.  
On appliquera alors séparément chaque règles - celle des dégâts normaux et celle des dégâts magiques, Puis on appliquera les règles d'armures correspondantes.


> Exemple avec *Excalibur (puissance: 200, arme légendaire)* :  
> Dégâts normaux (facteur critique : 110) : `200 * 110% = 220 dégâts`  
> Le joueur défenseur recevra 220 dégâts normaux, à appliquer selon la résistance de son armure  
> Dégâts magiques (facteur critique : 95) : `200 * 95% = 190 dégâts`  
> Le joueur défenseur recevra 190 dégâts magiques, à appliquer selon la résistance magique de son armure  
