# Travaux pratiques 

Objectifs : 
* écrire des tests en Java avec Junit
* exécuter les tests et exploiter les résultats

## Théorie

* du code qui teste du code
* automatisé : outil qui exécutera les tests et afficher les résultats
* intégration continue 
* en isolation : chaque test est indépendant des autres tests
* test = documentation
* attention à la LISIBILITE
* permet d'améliorer le code

## Consignes 

*On va utiliser Git durant le projet pour passer d'un exercice à l'autre.*  
*Commencez par cloner le projet :*  
```bash
git clone https://gitlab.com/cours-iut/2020-test-tp1-java.git -b master
git checkout -b tp-<votre_nom>
```
*Pensez à commiter entre chaque exercice !*  
*Allez au premier exercice :*  
```bash
git merge origin/exercice-1
```

## Un premier test

- par convention les tests sont dans `src/main/test`
- le package à une importance car les deux arborescences seront fusionnées lors de l'éxecution 
- la suite de tests est une simple classe Java
- un test unitaire est une méthode `void` sans paramètre annotée par `@Test`
- un test est décomposé en trois parties: Arrange-Act-Assert / Given-When-Then
- JUnit propose des assertions de base

*Allez dans ArmeTest.java*  

## Exécution et configuration du projet

*Allez voir la configuration dans le POM*

- lancement avec Maven : `mvn test`

*Lancez les tests avec Maven*  

- avec Intellij IDEA : 
  - utiliser le menu `Run` (`Maj+F10`)
  - permet de lancer le test en mode debug !
  
*Lancez les tests avec Intellij IDEA*   
*Mettez un point d'arrêt et lancez les tests en mode debug*  

----

*Allez à l'exercice 2*
```bash
git merge origin/exercice-2
```

### Les assertions de JUnit

- JUnit propose un ensemble complet d'assertions : [Assertions](https://junit.org/junit5/docs/5.0.1/api/org/junit/jupiter/api/Assertions.html)
- bien nommer ses variables ou ses méthodes pour faciliter la lecture

*Allez dans JoueurTest.java*  

### Before & After

- JUnit propose des annotations spécifiques pour exécuter du code en dehors des tests
- `@BeforeEach` et `@AfterEach` permettent d'éxécuter du code avant ou après chaque test
- `@BeforeAll` et `@AfterAll` permettent d'éxécuter du code avant ou après chaque suite de test

*Utilisez les annotations dans JoueurTest.java*

### Les assertions d'AssertJ

- AssertJ propose également un ensemble d'assertions : [AssertJ](https://assertj.github.io/doc)
- Assertions "fluent"
- la clarté des test est importante

*Ajoutez AssertJ dans le POM : [Maven Central](https://mvnrepository.com/artifact/org.assertj/assertj-core)*  
*Utilisez ces nouvelles assertions dans JoueurTest.java*  

----

*Allez à l'exercice 3*
```bash
git merge origin/exercice-3
``` 

## Les mocks

- Mockito permet de créer des faux objets
- on peut leur faire retourner ce que l'on veut (même des exceptions)
- on peut regarder s'ils ont été utilisés et ce qu'ils ont reçu comme paramètres
- facilite l'isolation des tests

*Allez voir la configuration dans le POM*  
*Allez dans TweetJoueurTest.java*  

## Le coverage

- le coverage mesure le code qui a été exécuté lors des tests
- il permet de visualiser les lignes ou instructions non testées
- Jacoco est une bibliothèque de mesure du coverage
- il s'intègre à Maven et permet de générer des rapports
- Jacoco crée plusieurs types de rapports
  - pour les outils (comme [Sonar](https://www.sonarqube.org) par exemple) : `target/jacoco.exec`
  - pour les humains : `target/sites/jacoco`

*Allez voir la configuration dans le POM*  
*Exécutez les tests avec Maven et visualisez le rapport HTML dans un navigateur*  
*Visualisez le rapport "outils" dans Intellij IDEA `Analyze > Show code coverage data`*   
*Exécutez les tests en utilisant l'outil de coverage d'Intellij IDEA `Run > Run with coverage`*  
