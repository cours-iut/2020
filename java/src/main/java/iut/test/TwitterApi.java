package iut.test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class TwitterApi {
    public void sendTweet(String message) {
        HttpClient httpClient = HttpClient.newHttpClient();

        try {
            httpClient.send(
                    HttpRequest.newBuilder()
                            .uri(URI.create("http://twitter.com/api"))
                            .POST(HttpRequest.BodyPublishers.ofString(message))
                            .build(),
                    HttpResponse.BodyHandlers.discarding()
            );
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Impossible d'envoyer le tweet !", e);
        }
    }
}
