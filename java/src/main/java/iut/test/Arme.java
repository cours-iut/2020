package iut.test;

import java.util.Objects;

class Arme {

    private final String name;
    private final Classe classeRequise;

    Arme(String name, Classe classeRequise) {
        this.name = name;
        this.classeRequise = classeRequise;
    }

    String getName() { return name; }
    Classe getClasseRequise() { return classeRequise; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arme arme = (Arme) o;
        return Objects.equals(name, arme.name) &&
                classeRequise == arme.classeRequise;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, classeRequise);
    }

    @Override
    public String toString() {
        return "Arme{" +
                "name='" + name + '\'' +
                ", classeRequise=" + classeRequise +
                '}';
    }
}