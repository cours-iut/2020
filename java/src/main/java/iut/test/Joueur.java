package iut.test;

import static iut.test.Classe.*;

class Joueur {
    private final String name;
    private final Classe classe;

    Joueur(String name, Classe classe) {
        this.name = name;
        this.classe = classe;
    }

    boolean canEquip(Arme arme) {
        if(classe == GUERRIER || classe == VOLEUR) {
            return arme.getClasseRequise() == GUERRIER || arme.getClasseRequise() == VOLEUR;
        }
        return arme.getClasseRequise() == classe;
    }

    public void sendTweet() {
        new TwitterApi().sendTweet("Rejoignez moi dans ce super RPG ! http://join.me.at.rpg/" + name);
    }
}
