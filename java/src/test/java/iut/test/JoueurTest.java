package iut.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JoueurTest {

    /*
     * 1. Implémentez les deux tests
     * 2. Utilisez des assertions JUnit plus précises
     * 3. Voyez-vous d'autres tests à implémenter ?
     */

    @Test
    void canEquip_armeGuerrier() {
        // Given
        Joueur kevinDu67 = new Joueur("KevinDu67", Classe.GUERRIER);
        Arme hacheDoubleTranchant = new Arme("Hache à double tranchant", Classe.GUERRIER);

        // When
        boolean isEquippable = kevinDu67.canEquip(hacheDoubleTranchant);

        // Then
        // assertEquals(isEquippable, _TODO_);
    }

    @Test
    void cannotEquip_armeMage() {
        // Given
        Joueur kevinDu67 = new Joueur("KevinDu67", Classe.GUERRIER);
        Arme sceptreDeFeu = new Arme("Sceptre de feu", Classe.MAGE);

        // When
        boolean isEquippable = kevinDu67.canEquip(sceptreDeFeu);

        // Then
        // assertEquals(isEquippable, _TODO_);
    }
}