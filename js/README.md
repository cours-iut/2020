# Travaux pratiques

Objectifs : 
* écrire des tests en Javascript avec Jest
* exécuter les tests et exploiter les résultats

## Théorie

* du code qui teste du code
* automatisé : outil qui exécutera les tests et afficher les résultats
* intégration continue 
* en isolation : chaque test est indépendant des autres tests
* test = documentation
* attention à la LISIBILITE
* permet d'améliorer le code

## Consignes 

*On va utiliser Git durant le projet pour passer d'un exercice à l'autre.*  
*Commencez par cloner le projet :*  
```bash
git clone https://gitlab.com/cours-iut/2020-test-tp1-js.git -b master
git checkout -b tp-<votre_nom>
```
*Pensez à commiter entre chaque exercice !*  
*Allez à la première partie du TP :*  
```bash
git merge origin/exercice-1
```

## Un premier test

- Jest est une des plus simple et plus complète des bibliothèque de test en Javascript 
- par convention, un fichier de test termine par `.spec.js`
- les fichiers de tests peuvent être placés dans un dossier à part comme avec Maven
- ils peuvent aussi être placé à côté du fichier à tester
- une suite de tests est un simple fichier javascript
- un test consiste en un appel de la fonction `test`
- Jest propose des assertions dans le style d'AssertJ

*Allez dans arme.spec.js*  

## Exécution et configuration du projet

*Allez voir la configuration dans le fichier package.json*

- lancement avec NPM : `npm run test`
- lancement avec NPX : `npx jest`
- lancer un serveur de test avec l'option `--watch`

----

*Allez à l'exercice 2*
```bash
git merge origin/exercice-2
```

## Les assertions de Jest

- Jest propose de nombreuses assertions : [Jest : expect](https://jestjs.io/docs/en/expect.html)
- bien nommer ses variables et ses méthodes pour faciliter la lecture

*Allez dans joueur.spec.js*  

## Before & After

- Jest propose des méthodes spécifiques pour exécuter du code en dehors des tests
- `beforeEach()` et `afterEach()` permettent d'éxécuter du code avant ou après chaque test
- `beforeAll()` et `afterAll()` permettent d'éxécuter du code avant ou après chaque suite de test

*Utilisez ces méthodes dans joueur.spec.js*

----

*Allez à l'exercice 3*
```bash
git merge origin/js-exercice-3
```

## Les mocks

- Jest permet de créer des faux objets
- on peut leur faire retourner ce que l'on veut (même des erreurs)
- on peut regarder s'ils ont été utilisés et ce qu'ils ont reçu comme paramètres
- cela permet de faciliter les tests en isolation

*Allez dans joueur-tweet.spec.js*  

## Le coverage

- le coverage mesure le code qui a été exécuté lors des tests
- il permet de visualiser les lignes ou instructions non testées
- Istanbul est une bibliothèque de mesure du coverage
- il permet de générer des rapports
- Jest intègre Instanbul de manière transparente

*Allez voir la configuration dans le package.json*  
*Exécutez les tests avec NPM*  
*Allez voir le rapport HTML dans le dossier `coverage`*
 