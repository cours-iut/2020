import { GUERRIER, VOLEUR } from "./classe";
import TwitterApi from './twitter';

class Joueur {
    constructor(name, classe) {
        this.name = name;
        this.classe = classe;
        this.inventaire = [];
    }

    canEquip(arme) {
        if (this.classe === GUERRIER || this.classe === VOLEUR) {
            return arme.classeRequise === GUERRIER || arme.classeRequise === VOLEUR;
        }
        return arme.classeRequise === this.classe;
    }

    tweet() {
        new TwitterApi().sendTweet(`Rejoignez moi dans ce super RPG ! http://join.me.at.rpg/${this.name}`);
    }
}

export default Joueur;