import Joueur from "./joueur"
import { GUERRIER } from "./classe";

describe("joueur > tweeter", () => {

    /*
     * 1. Implémentez le test
     */

    it("envoie un tweet", () => {
        // Given
        const kevinDu67 = new Joueur("kevinDu67", GUERRIER);

        // When
        kevinDu67.tweet();

        // Then
        expect(null).toEqual("TODO")
    });
})