import Joueur from "./joueur";
import Arme from "./arme";
import { GUERRIER, MAGE } from "./classe";

describe("joueur", () => {

    /*
     * 1. Implémentez les tests
     * 2. Ajouter un nouveau test avec une arme pour VOLEUR
     */

    it("peut équiper une arme de guerrier", () => {
        // Given
        const kevinDu67 = new Joueur("KevinDu67", GUERRIER);
        const hacheDoubleTranchant = new Arme("Hache a double tranchant", GUERRIER);

        // When
        const isEquippable = kevinDu67.canEquip(hacheDoubleTranchant);

        // Then
        // expect(isEquippable).toEqual(_TODO_);
    });

    it("ne peut pas équipper une arme de mage", () => {        
        // Given
        const kevinDu67 = new Joueur("KevinDu67", GUERRIER);
        const sceptreDeFeu = new Arme("Sceptre de feu", MAGE);

        // When
        const isEquippable = kevinDu67.canEquip(sceptreDeFeu);

        // Then
        // expect(isEquippable).toEqual(_TODO_);
    });
});