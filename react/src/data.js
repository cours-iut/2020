const inventaire = [{
  id: "a23f",
  nom: "Faux d'Elune",
  classe: "Druide",
  description: "Une faux bénie par Elune."
}, {
  id: "56dd",
  nom: "Dague empoisonnée",
  classe: "Voleur",
  description: "Enduite d'un venin très toxique."
},{
  id: "8f1c",
  nom: "Hache à double tranchant",
  classe: "Guerrier",
  description: "Elle ne sert pas à couper du bois."
},{
  id: "b3d2",
  nom: "Baguette en frêne",
  classe: "Mage",
  description: "Achetée chez Ollivander."
},{
  id: "e5a2",
  nom: "Claymore",
  classe: "Guerrier",
  description: "Une épée particulièrement lourde."
}];

const joueur = {
  nom: "KevinDu67",
  classe: "Guerrier",
  armeEquipee: '8f1c',
  inventaire,
}

export { joueur };