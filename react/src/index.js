import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './App';
import rootReducer from './store/reducers';
import { initJoueur } from './store/actions';
import { joueur } from './data';
import 'milligram';
import './index.css';

const store = createStore(rootReducer);
store.dispatch(initJoueur(joueur));

ReactDOM.render(
    <Provider store={store}>
        <App joueur={joueur} />
    </Provider>,
    document.getElementById('root')
);
