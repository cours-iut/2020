import { combineReducers } from "redux";

export const joueur = (state = {}, action) => {
    switch (action.type) {
        case 'INIT_JOUEUR': {
            return action.joueur;
        }
        case 'EQUIP_ARME':
            return {
                ...state,
                armeEquipee: (state.classe === action.arme.classe) ? action.arme.id : state.armeEquipee,
            };
        default:
            return state;
    }
};

export default combineReducers({
    joueur
});
