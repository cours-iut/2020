export const equip = arme => ({
    type: 'EQUIP_ARME',
    arme: arme,
});

export const initJoueur = joueur => ({
    type: 'INIT_JOUEUR',
    joueur,
});