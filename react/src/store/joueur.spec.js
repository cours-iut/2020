import { equip, initJoueur } from './actions'
import { joueur as reducer } from './reducers'

describe('joueur reducers', () => {

    const armes = {
        epeeCourte: {
            id: '1e28',
            nom: 'Epée courte',
            classe: 'Guerrier',
            description: 'Parfaite pour les combats rapprochés.'
        },
        marteauDeBronze: {
            id: 'c5b1',
            nom: 'Marteau en bronze',
            classe: 'Guerrier',
            description: 'Capable de détruire les crânes aussi bien que les murs.'
        },
        arbalete: {
            id: 'b25f',
            nom: 'Arbalète',
            classe: 'Chasseur',
            description: 'Une arme capable de toucher une pomme à 300m.'
        },
    }

    const joueur = {
        nom: 'KevinDu67',
        classe: 'Guerrier',
        armeEquipee: '1e28',
        inventaire: [armes.epeeCourte, armes.marteauDeBronze]
    }

    describe('init joueur', () => {

        /*
         * 1. Implémentez le test
         */

        it("initialise le store", () => {
            expect(reducer('_TODO_', '_TODO_')).toEqual(joueur);
        });
    });

    describe('equip', () => {

        /*
         * 2. Implémentez les tests
         * 3. Décommentez le dernier test et implémentez-le.
         */

        it("équipe une arme", () => {
            const newState = reducer(joueur, '__TODO__');
            expect(newState.armeEquipee).toEqual('__TODO__');
        });

        it("n'équipe pas une arme d'une autre classe", () => {
            const newState = reducer(joueur, '__TODO__');
            expect(newState.armeEquipee).toEqual('__TODO__');
        });

        /*
        it("n'équipe pas une arme absente de l'inventaire", () => {
            const hallebarde = {
                id: '55fe',
                nom: 'Hallebarde',
                classe: 'Guerrier',
                description: "L'arme de prédilection des gardes."
            };

            const newState = reducer(joueur, '__TODO__');
            expect(newState.armeEquipee).toEqual('__TODO__');
        });
        */
    })
});