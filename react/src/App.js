import React from 'react';
import './App.css';
import JoueurContainer from './game/JoueurContainer';

function App({joueur}) {
  return (
    <div className="app">
      <div className="container">
        <div className="row">
          <div className="column">
            <JoueurContainer
              data-testid="joueur"
              joueur={joueur}
            ></JoueurContainer>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
