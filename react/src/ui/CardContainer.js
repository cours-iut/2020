import React from 'react';

function CardContainer({children, ...rest}) {
    return (
        <div className="container">
            <div className="row">
                <div className="column">
                    <div className="card-container" {...rest}>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CardContainer;