import React from 'react';

function Card({children, ...rest}) {
    return (
        <div className="card" {...rest}>
            {children}
        </div>
    );
}

export default Card;
