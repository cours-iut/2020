import React from 'react';
import CardContainer from '../ui/CardContainer';
import Card from '../ui/Card'

function Joueur({ joueur, onSelectArme, ...attributes }) {
    const { nom, classe, armeEquipee, inventaire } = joueur;
    return (
        <div {...attributes}>
            <h1>{nom}</h1>
            <h3 data-testid="classe">{classe}</h3>
            <hr />
            <ul>
                {inventaire.map(arme => {
                    const isEquipee = (armeEquipee === arme.id);
                    return (
                        <li key={arme.id} data-testid={`inventaire-arme-${arme.id}`}>
                            {isEquipee ? (
                                <button class="button button-outline" disabled="true">équippé</button>
                            ) : (
                                    <button class="button button-outline" onClick={() => onSelectArme(arme)}>équipper</button>
                                )}&nbsp;
                            ({arme.classe})
                        {arme.nom}
                            &nbsp;-&nbsp;
                        <em>{arme.description}</em>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

export default Joueur;