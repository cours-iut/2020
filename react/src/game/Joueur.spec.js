import React from 'react';
import renderer from 'react-test-renderer';
import Joueur from './Joueur';

describe('Joueur', () => {

    /*
     * 1. Implémentez et lancez le test
     * 2. Utilisez un <h2> pour afficher la classe du joueur, puis relancez le test
     * 3. Acceptez le nouveau snapshot
     */

    it('match snapshot', () => {
        const joueur = {
            nom: 'kevinDu67',
            classe: 'Guerrier',
            inventaire: [{
                id: '1e8d',
                nom: "Epée courte",
                classe: "Guerrier",
                description: "Maniable et équilibrée."
            }]
        }
        // expect(renderer.create(/* TODO */).toJSON()).toMatchSnapshot();
    })
})