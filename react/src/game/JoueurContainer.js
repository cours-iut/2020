import { connect } from "react-redux";
import { equip } from '../store/actions'
import Joueur from "./Joueur";

const mapStateToProps = (state, ownProps) => ({
    joueur: state.joueur,
    'data-testid': ownProps['data-testid'],
});

const mapDispatchToProps = dispatch => ({
    onSelectArme: arme => { dispatch(equip(arme) )}
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Joueur);