import React from 'react';
import { render } from '@testing-library/react';
import Joueur from './Joueur';

describe('Joueur', () => {

    /*
     * 1. Implémentez et lancez les tests
     * 2. Allez dans Joueur.js et mettez la classe du joueur dans un `h2`, puis relancez les tests
     * 3. Implémentez un test vérifiant que l'inventaire est bien affiché
     * 4. Utilisez les composants `CardContainer` et `Card` pour affichez les objets de l'inventaire, puis relancez les tests
     */

    const joueur = {
        nom: 'kevinDu67',
        classe: 'Guerrier',
        inventaire: [{
            id: '1e8d',
            nom: "Epée courte",
            classe: "Guerrier",
            description: "Maniable et équilibrée.",
        }, {
            id: 'f52e',
            nom: 'Arc court sylvestre',
            classe: 'Chasseur',
            description: 'Orné de fines gravures.',
        }]
    };

    it('affiche la classe du joueur', () => {
        const { getByTestId } = render(<Joueur joueur={joueur}/>);
        
        const classe = getByTestId('_TODO_');

        expect(classe.innerHTML).toEqual(expect.stringContaining('Guerrier'));
    });

    it("affiche l'inventaire", () => {
        const { getAllByTestId } = render(<Joueur joueur={joueur}/>);

        const items = getAllByTestId(/__TODO__/);

        expect(items.length).toEqual(2);

        const ids = items
            .map(it => it.attributes)
            .map(attrs => attrs['data-testid'])
            .map(attr => attr.value)
            .map(testId => testId.replace('__TODO__', ''))
        expect(ids).toEqual(['1e8d', 'f52e']);
    })
})