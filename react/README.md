# Travaux pratiques

Objectifs:
- tester unitairement dans React

# Théorie

- React est une bibliothèque dévelopée par Facebook permettant de créer des composants en javascript
- Jest, également développé par Facebook et React Testing Library permettent de facilement tester les composants

## Consignes

*On va utiliser Git durant le projet pour passer d'un exercice à l'autre.*  
*Commencez par cloner le projet*  
```bash
git clone https://gitlab.com/cours-iut/2020-test-tp1-react.git -b master
git checkout -b tp-<votre_nom>
```

*Pensez à commiter entre chaque exercice !*  
*Allez au premier exercice :*  
```bash
git merge origin/exercice-1
```

## Les tests avec "snapshots"

- Jest permet de générer des snapshots des composants
- il s'agit d'une sauvegarde sous forme de fichier du rendu
- le premier test génère et sauvegarde le snapshot pour l'utiliser comme référence
- les tests suivants génèrent le snapshot et le compare au snapshot de référence
- si le nouveau snapshot est différent, le test échoue et affiche un DIFF
- si ce nouveau snapshot est correct, il est possible de le sauvegarder comme nouveau snapshot de référence

*Allez dans Joueur.spec.js*  

- inconvénient : le moindre changement du rendu html fait échouer le test
- problème : imaginez l'effet d'une migration vers une nouvelle version de Bootstrap dans un gros projet !

----

*Allez à l'exercice 2*
```bash
git merge origin/exercice-2
```

## Les tests avec des attributs "data-test"

- pour découpler le test du html, on va placer des attributs spécifiques
- par convention on utilise le préfixe `data-test`
- on va utiliser la bibliothèque React Testing pour faciliter les tests : [React Testing API](https://testing-library.com/docs/react-testing-library/api)
- la bibliothèque propose d'utiliser l'attribut `data-testid` et met à disposition des méthodes pour trouver ces éléments

*Allez dans Joueur.exercice2.spec.js*  

----

*Allez à l'exercice 3*
```bash
git merge origin/exercice-3
```

## Les tests avec Redux

- Redux est la bibliothèque la plus répandue pour gérer des données dans une application React : [Redux](https://redux.js.org/introduction/getting-started)
- avec Redux, on va disposer d'un "store" global pour stocker et accéder à nos données
- des actions pourront être envoyées au store (`dispatch`)
- ces actions seront traitées pour mettre à jour le store (`reducers`)
- il est intéressant de tester les reducers car ils vont rapidement contenir du code métier

*Allez dans store/joueur.spec.js*

- à noter : Redux propose dans [son tutoriel React](https://redux.js.org/basics/usage-with-react) des bonnes pratiques, comme la séparation des composants "d'affichage" des composants intéragissant avec les données.